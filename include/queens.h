#ifndef QUEENS_h_
#define QUEENS_h_

#define BOARD_SIZE  8
#define CELL_SIZE   75

typedef unsigned long int Board;

int     conflict(Board board, int pos);

#endif
