#ifndef BIT_BOARD_h_
#define BIT_BOARD_h_

#include <stdio.h>

#include "queens.h"

int     bit_value_ULI(Board n, int position);
void    print_ULI(Board n);
void    set_positive_bit_ULI(Board* n, int position);
void    set_negative_bit_ULI(Board* n, int position);
void    reset_all_bit_ULI(Board* n);
int     count_positive_bit_ULI(Board* n);

#endif
