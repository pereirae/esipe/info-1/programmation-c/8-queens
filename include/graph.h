#ifndef GRAPH_h_
#define GRAPH_h_

#include "sdl-wrapper/sdl_wrapper.h"
#include "queens.h"

#define PLAYING 0
#define LOOSE 1
#define WIN 2
#define EXIT 3

void draw_board(SDL_ressources *sdl_r, Board board, int board_size, int cell_size, SDL_Texture *queen);
void drawOverlay(SDL_ressources *sdl_r, int state, int width, int height);

#endif
