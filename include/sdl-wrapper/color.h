#ifndef COLOR_h_
#define COLOR_h_

#include <stdlib.h>
#include "SDL2/SDL.h"

/**
 * Create a new color
 *
 * \param r The red value of the new color
 * \param g The green value of the new color
 * \param b The blue value of the new color
 * \param a The opacity of the new color
 * \return SDL_Color* A pointer on the new color
 */
SDL_Color *create_color(uint8_t r, uint8_t g, uint8_t b, uint8_t a);

/**
 * Destroy a color
 *
 * @param color A pointer on the color to destroy
 */
void destroy_color(SDL_Color *color);

/**
 * Sauvegarde la couleur actuelle du renderer
 *
 * \param renderer le renderer dont la couleur doit être sauvegardée
 * \param color la structure SDL_Color dans laquelle la coueleur est sauvegardée
 */
void getCurrentRenderColor(SDL_Renderer *renderer, SDL_Color *color);

/**
 * Change la couleur de dessin du renderer
 *
 * \param renderer le renderer dont la couleur est à changer
 * \param color la nouvelle couleur de dessin du renderer
 */
void setCurrentRenderColor(SDL_Renderer *renderer, SDL_Color *color);
#endif
