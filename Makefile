NAME		= Queens

#COMPILER
CC			= gcc
FLAGS		= -Wall -pedantic
WRAPPER		= SDL_wrapper

#FOLDERS
SRC			= ./src
OBJ			= ./obj
INC			= ./include
LIB			= ./lib

#OBJ FILES
OBJS		= $(OBJ)/main.o $(OBJ)/bit_board.o $(OBJ)/graph.o $(OBJ)/queens.o

$(NAME): create_obj_dir $(OBJS)
	$(CC) -o $@ $(OBJS) $(FLAGS) -I$(INC) -L$(LIB) -lSDL2 -lSDL2_ttf -lSDL2_image -l$(WRAPPER) -Wl,-rpath,$(LIB)
	install_name_tool -change libSDL_wrapper.dylib lib/libSDL_wrapper.dylib Queens

##Create the obj dir if needed
create_obj_dir:
	mkdir -p $(OBJ)

$(OBJ)/main.o: $(SRC)/main.c

$(OBJ)/%.o: $(SRC)/%.c
	$(CC) -c $< -ansi $(FLAGS) -o $@

clean:
	rm -rf $(OBJS)

fclean: clean
	rm -rf $(NAME)

re: fclean $(NAME)

.PHONY: all clean fclean re
