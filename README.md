# Queens

## Troubleshooting
### MacOS X

If you encounter the following error :
```bash
dyld[49423]: Library not loaded: libSDL_wrapper.dylib
```

Execute the following command :
```bash
install_name_tool -change libSDL_wrapper.dylib lib/libSDL_wrapper.dylib Queens
```

You should now be able to run `./Queens`.