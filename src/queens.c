#include "../include/queens.h"
#include "../include/bit_board.h"

int check_diag_top_left(Board board, int pos) {
    int count;

    count = 0;
    while ((pos % 8) != 0) {
        pos -= 9;
        count += bit_value_ULI(board, pos);

    }
    return count;
}

int check_diag_top_right(Board board, int pos) {
    int count;

    count = 0;
    while (((pos + 1) % 8) != 0) {
        pos -= 7;
        count += bit_value_ULI(board, pos);
    }
    return count;
}

int check_diag_down_left(Board board, int pos) {
    int count;

    count = 0;
    while ((pos % 8) != 0) {
        pos += 7;
        count += bit_value_ULI(board, pos);
    }
    return count;
}

int check_diag_down_right(Board board, int pos) {
    int count;

    count = 0;
    while (((pos + 1) % 8 != 0) && pos < (sizeof(board) * 8)) {
        pos += 9;
        count += bit_value_ULI(board, pos);
    }
    return count;
}

int check_diag(Board board, int pos) {
    int count;

    count = 0;
    count += check_diag_down_left(board, pos);
    count += check_diag_down_right(board, pos);
    count += check_diag_top_left(board, pos);
    count += check_diag_top_right(board, pos);

    return count;
}

int check_col(Board board, int pos) {
    int i;
    int start_pos;
    int count;

    if (pos > 8) {
        start_pos = pos % 8;
    } else {
        start_pos = pos;
    }

    count = 0;
    for (i = 0; i < 8; i++) {
        if (bit_value_ULI(board, start_pos + (i * 8))) {
            count++;
        }
    }
    return count > 1;
}

int check_line(Board board, int pos) {
    int i;
    int start_pos;
    int count;

    if (pos > 8) {
        start_pos = pos - (pos % 8);
    } else {
        start_pos = 0;
    }
    count = 0;
    for (i = 0; i < 8; i++) {
        if (bit_value_ULI(board, start_pos + i)) {
            count++;
        }
    }
    return count > 1;
}

int conflict(Board board, int pos) {
    int col = 0;
    int line = 0;
    int diag = 0;

    col = check_col(board, pos);
    line = check_line(board, pos);
    diag = check_diag(board, pos);
    return col || line || diag;
}
