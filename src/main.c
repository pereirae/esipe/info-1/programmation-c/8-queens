#include "../include/bit_board.h"
#include "../include/sdl-wrapper/sdl_wrapper.h"
#include "../include/graph.h"

int handleMouseEvent(Board *board) {
    int     x;
    int     y;
    int     pos;

    SDL_GetMouseState(&x, &y);

    x = x / CELL_SIZE;
    y = y / CELL_SIZE;
    pos = 8 * y + x;
    set_positive_bit_ULI(board, pos);
    return conflict(*board, pos);
}

int handleKeyUp(int previous_state, SDL_KeyboardEvent event, Board *board) {
    if (event.keysym.scancode == SDL_SCANCODE_O) {
        reset_all_bit_ULI(board);
        return PLAYING;
    }
    if (event.keysym.scancode == SDL_SCANCODE_N) {
        return EXIT;
    }
    return previous_state;
}

void gameLoop(SDL_ressources *sdl_r, int board_w, int board_h) {
    SDL_Color           *background = NULL;
    SDL_bool            quit = SDL_FALSE;
    SDL_Texture         *queen;
    SDL_Surface         *tmp;
    unsigned long int   board = 0;
    int                 state = 0;

    if (!(background = create_color(137, 140, 145, 255))) {
        return;
    }

    tmp = IMG_Load("./assets/img/queen.png");
    if (!tmp) {
        fprintf(stderr, "Error while loading assets : %s\n", SDL_GetError());
        return;
    }
    queen = SDL_CreateTextureFromSurface(sdl_r->renderer, tmp);
    if (!queen) {
        fprintf(stderr, "Error while creating texture : %s\n", SDL_GetError());
        return;
    }
    SDL_FreeSurface(tmp);
    setCurrentRenderColor(sdl_r->renderer, background);

    while (!quit && state != EXIT) {
        while (SDL_PollEvent(&sdl_r->event)) {
            switch(sdl_r->event.type) {
                case SDL_QUIT:
                    quit = SDL_TRUE;
                    break;
                case SDL_MOUSEBUTTONUP:
                    if (state == PLAYING) {
                        state = handleMouseEvent(&board);
                        if (count_positive_bit_ULI(&board) == 8) {
                            state = WIN;
                        }
                    }
                    break;
                case SDL_KEYUP:
                    if (state == WIN || state == LOOSE)
                    state = handleKeyUp(state, sdl_r->event.key, &board);
                default:
                    break;
            }
        }

        SDL_RenderClear(sdl_r->renderer);
        draw_board(sdl_r, board, BOARD_SIZE, CELL_SIZE, queen);
        if (state == WIN || state == LOOSE) {
            drawOverlay(sdl_r, state, board_w, board_h);
        }
        SDL_RenderPresent(sdl_r->renderer);
    }
    destroy_color(background);
}

int main(int argc, char** argv) {
    int             board_w;
    int             board_h;
    SDL_ressources  *sdl_ressources = NULL;

    board_w = BOARD_SIZE * CELL_SIZE + CELL_SIZE / 2;
    board_h = (BOARD_SIZE + 1) * CELL_SIZE - CELL_SIZE / 2;

    if ((sdl_ressources = setup_SDL("8 Queens", board_h, board_w)) == NULL) {
        return 1;
    }
    gameLoop(sdl_ressources, board_w, board_h);
    exit_SDL(sdl_ressources);
    return 0;
}
