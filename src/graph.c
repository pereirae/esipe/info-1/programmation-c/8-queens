#include "../include/graph.h"
#include "../include/bit_board.h"
#include <string.h>

void draw_chess_cell(Coord pos, int size, SDL_Renderer *renderer, SDL_Color *color) {
    SDL_Rect    rect;
    SDL_Color   *current_color = NULL;

    if ((current_color = malloc(sizeof(SDL_Color))) == NULL) {
        return;
    }
    rect.x = pos.x; rect.y = pos.y; rect.h = size; rect.w = size;
    getCurrentRenderColor(renderer, current_color);
    setCurrentRenderColor(renderer, color);
    SDL_RenderFillRect(renderer, &rect);
    setCurrentRenderColor(renderer, current_color);
    free(current_color);
    return;
}

void draw_queen(SDL_ressources *sdl_r, Coord pos, int cell_size, SDL_Texture *texture) {
    SDL_Rect rect;

    rect.x = pos.x + (cell_size * 0.05);
    rect.y = pos.y + (cell_size * 0.05);
    rect.w = cell_size - (cell_size * 0.1);
    rect.h = cell_size - (cell_size * 0.1);
    SDL_RenderCopy(sdl_r->renderer, texture, NULL, &rect);
}

void draw_cell_identifiers(SDL_ressources *sdl_r, int board_size, int cell_size) {
    int         i;
    char        letter[2];
    TTF_Font    *font = NULL;
    SDL_Color   *color = NULL;
    Text        *text = NULL;
    SDL_Rect    rect;

    letter[0] = 'a';
    letter[1] = '\0';
    rect.w = cell_size / 7;
    rect.h = cell_size / 4;

    if (!(color = create_color(0, 0, 0, 255))) {
        return;
    }

    if (!(font = TTF_OpenFont("assets/fonts/Roboto-Light.ttf", 12))) {
        return;
    }

    for (i = 0; i < board_size; i++) {
        rect.x = i * cell_size + cell_size / 3;
        rect.y = cell_size * board_size + cell_size / 4 - rect.h / 2;
        if (!(text = create_text(letter, font, *color, sdl_r->renderer))) {
            continue;
        };
        SDL_RenderCopy(sdl_r->renderer, text->texture, NULL, &rect);
        destroy_text(text);
        letter[0]++;
    }

    letter[0] = '0';
    for (i = 0; i < board_size; i++) {
        rect.x = cell_size * board_size + cell_size / 4 - rect.w / 2;
        rect.y = i * cell_size + cell_size / 3;
        if (!(text = create_text(letter, font, *color, sdl_r->renderer))) {
            continue;
        };
        SDL_RenderCopy(sdl_r->renderer, text->texture, NULL, &rect);
        destroy_text(text);
        letter[0]++;
    }

    TTF_CloseFont(font);
    destroy_color(color);
}

void draw_board_border(SDL_ressources *sdl_r, int board_size, int cell_size) {
    SDL_Color   *white = NULL;
    SDL_Color   *black = NULL;
    SDL_Color   *current_col = NULL;
    SDL_Rect    rect;

    if (!(white = create_color(255, 255, 255, 255))) {
        return;
    }

    if (!(black = create_color(0, 0, 0, 255))) {
        return;
    }

    if (!(current_col = (SDL_Color*)malloc(sizeof(SDL_Color)))) {
        return;
    }
    getCurrentRenderColor(sdl_r->renderer, current_col);
    setCurrentRenderColor(sdl_r->renderer, white);
    rect.h = cell_size * board_size + cell_size / 2;
    rect.w = cell_size * board_size + cell_size / 2;
    rect.x = 0;
    rect.y = 0;
    SDL_RenderFillRect(sdl_r->renderer, &rect);

    setCurrentRenderColor(sdl_r->renderer, black);

    /* Board bottom */
    SDL_RenderDrawLine(sdl_r->renderer,
        0,
        cell_size * board_size,
        cell_size * board_size,
        cell_size * board_size);

    /* Board side */
    SDL_RenderDrawLine(sdl_r->renderer,
        cell_size * board_size,
        0,
        cell_size * board_size,
        cell_size * board_size);

    /* Board bottom + padding */
    SDL_RenderDrawLine(sdl_r->renderer,
        0,
        cell_size * board_size + cell_size / 2,
        cell_size * board_size + cell_size / 2,
        cell_size * board_size + cell_size / 2);

    /* Board side + padding */
    SDL_RenderDrawLine(sdl_r->renderer,
        cell_size * board_size + cell_size / 2,
        0,
        cell_size * board_size + cell_size / 2,
        cell_size * board_size + cell_size / 2);

    setCurrentRenderColor(sdl_r->renderer, current_col);
    destroy_color(white);
    destroy_color(black);
}

void draw_board(SDL_ressources *sdl_r, Board board, int board_size, int cell_size, SDL_Texture *queen) {
    int         i;
    Coord       pos;
    SDL_Color   *cell_color = NULL;
    SDL_Color   *color_white = NULL;
    SDL_Color   *color_grey = NULL;
    SDL_Color   *color_green = NULL;

    if (!(color_white = create_color(255, 255, 255, 255))) {
        return;
    }
    if (!(color_grey = create_color(117, 117, 117, 255))) {
        return;
    }
    if (!(color_green = create_color(0, 255, 0, 255))) {
        return;
    }

    draw_board_border(sdl_r, board_size, cell_size);
    draw_cell_identifiers(sdl_r, board_size, cell_size);

    for (i = 0; i < sizeof(Board) * 8; i++) {
        cell_color = (i + (i / 8)) % 2 == 0 ? color_white : color_grey;
        pos.x = (i % board_size) * cell_size;
        pos.y = (i / board_size) * cell_size;
        draw_chess_cell(pos, cell_size, sdl_r->renderer, cell_color);
        if (bit_value_ULI(board, i)) {
            draw_queen(sdl_r, pos, cell_size, queen);
        }
    }

    destroy_color(color_green);
    destroy_color(color_grey);
    destroy_color(color_white);

    return;
}

void drawText(SDL_ressources *sdl_r, char *text_content, TTF_Font *font,SDL_Color *color, int x, int y) {
    Text        *text = NULL;
    SDL_Rect    text_rect;
    text_rect.w = strlen(text_content) * 12;
    text_rect.h = 24;
    text_rect.x = x - text_rect.w / 2;
    text_rect.y = y - text_rect.h / 2;
    if (!(text = create_text(text_content, font, *color, sdl_r->renderer))) {
        return;
    }
    SDL_RenderCopy(sdl_r->renderer, text->texture, NULL, &text_rect);
    destroy_text(text);
}

void drawOverlay(SDL_ressources *sdl_r, int state, int width, int height) {
    SDL_Color   *overlay = NULL;
    SDL_Color   *curr_color = NULL;
    SDL_Color   *white = NULL;
    SDL_Rect    rect;
    TTF_Font    *font = NULL;
    char        win[] = "Vous avez gagné !";
    char        loose[] = "Vous avez perdu :(";
    char        replay[] = "Rejouer ? Oui (o) Non (n)";

    if (!sdl_r && state != WIN && state != LOOSE) {
        return;
    }
    if ((curr_color = malloc(sizeof(SDL_Color))) == NULL) {
        return;
    }
    if (!(overlay = create_color(110, 107, 100, 192))) {
        return;
    }
    if (!(white = create_color(255, 255, 255, 255))) {
        return;
    }
    if (!(font = TTF_OpenFont("assets/fonts/Roboto-Light.ttf", 12))) {
        return;
    }
    getCurrentRenderColor(sdl_r->renderer, curr_color);

    rect.x = 0; rect.y = 0; rect.w = width; rect.h = height;
    setCurrentRenderColor(sdl_r->renderer, overlay);
    SDL_SetRenderDrawBlendMode(sdl_r->renderer, SDL_BLENDMODE_BLEND);
    SDL_RenderFillRect(sdl_r->renderer, &rect);

    if (state == LOOSE) {
        drawText(sdl_r, loose, font, white, width / 2, height / 2);
    } else {
        drawText(sdl_r, win, font, white, width / 2, height / 2);
    }
    drawText(sdl_r, replay, font, white, width / 2, height * 0.7);

    setCurrentRenderColor(sdl_r->renderer, curr_color);
    free(curr_color);
    destroy_color(white);
    destroy_color(overlay);
    TTF_CloseFont(font);
}
