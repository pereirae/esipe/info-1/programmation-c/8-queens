#include "../include/bit_board.h"

int coord_to_pos(int x, int y) {
    int position = -1;

    position = y * BOARD_SIZE + x;
    if (position > BOARD_SIZE * BOARD_SIZE) {
        return -1;
    }
    return position;
}

/* Bien caster les constantes quand elles sont plus petites que le type "lu" */
int bit_value_ULI(Board n, int position) {
    if (!(position >= 0 && position <= sizeof(n) * 8)) {
        return 0;
    }
    return (n&((Board)1<<position) ? 1 : 0);
}

void print_ULI(Board n) {
    int i;
    for (i = (sizeof(n) * 8) - 1; i >= 0; i--) {
        putchar(bit_value_ULI(n, i) ? '1' : '0');
    }
    putchar('\n');
}

void set_positive_bit_ULI(Board* n, int position) {
    *n = (*n)|((Board)1<<position);
}

void set_negative_bit_ULI(Board* n, int position) {
    *n = (*n)&~((Board)1<<position);
}

void reset_all_bit_ULI(Board* n) {
    int i;
    for (i = 0; i < sizeof(n) * 8; i++) {
        set_negative_bit_ULI(n, i);
    }
}

int count_positive_bit_ULI(Board* n) {
    int i;
    int count;

    count = 0;
    for (i = 0; i < sizeof(n) * 8; i++) {
        count += bit_value_ULI(*n, i);
    }
    return count;
}
